const trelloId = "00RUQv18";
const key = "f4471a335d4de422182f52d9e48f51dd";
const tokenKey = "75a413038ce2a7f7986c3e66f2517fd84bb1af6251ce16458a82b8bc6b8229eb";
const boardId = "5cf5ffb9fe15522460c9c0dd";
const listId = "5d283460105c823a6ac5c8cf";
fetch('https://api.trello.com/1/boards/' + trelloId + '?name=myTrello&key=' + key + '&token=' + tokenKey)
    .then((res) => res.json())
    .then((data) => {
        //console.log(data)

    })
fetch('https://api.trello.com/1/boards/' + boardId + '/lists?name=myTrello&key=' + key + '&token=' + tokenKey)
    .then((res) => res.json())
    .then((data) => {
         // console.log(data)

    })
  async function gettingCardListId(){
    return await fetch('https://api.trello.com/1/lists/' + listId + '/cards?name=myTrello&key=' + key + '&token=' + tokenKey)
      .then(function(response){
          return response.json()
      })
     
  }
   async function insertingCards(inputText){
      return await fetch(('https://api.trello.com/1/cards?name=' + inputText + '&desc=trello+&pos=bottom&idList=' + listId + '&keepFromSource=all&key=' + key + '&token=' + tokenKey), {
        method: "POST",
    })
    .then((res) => {
       return res.json();

   }  )
}  
        async function gettingChecklist(cardId){
            return await  fetch('https://api.trello.com/1/cards/' +cardId + '/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate%2Ctype&filter=all&fields=all&key=' + key + '&token=' + tokenKey)
            .then((res) =>
            {
                return res.json();
            })
        }
      async function deleteCheckList(checkListId){
         return  await fetch(('https://api.trello.com/1/checklists/' +checkListId + '?key=' + key + '&token=' + tokenKey), {
            method: 'DELETE',
        })
        .then((res)=>{
            return res.json();
        })
      }
      async function insertingChecklist(cardId,inputValue){
      return await  fetch(('https://api.trello.com/1/cards/' +cardId + '/checklists?name=' + inputValue + '&pos=bottom&checked=false&key=' + key + '&token=' + tokenKey), {
            method: 'POST',
        })
       .then((res)=>{
            return  res.json();
        })
      }
      async function gettingListItems(checkListId)
      {
          return await  fetch('https://api.trello.com/1/checklists/' + checkListId + '/checkItems?&key=' + key + '&token=' + tokenKey)
          .then((res) =>{ 
          return res.json();
        })
         
      }
      async function insertingChecklistItems(checklistId,inputValue){
      
        return await  fetch(('https://api.trello.com/1/checklists/' +checklistId + '/checkItems?name=' + inputValue + '&pos=bottom&checked=false&key=' + key + '&token=' + tokenKey), {
            method: 'POST',
        })
       .then((res)=>{
            return  res.json();
        })
      }
      async function deleteCheckListItems(checkListId,listId){
        return await fetch(('https://api.trello.com/1/checklists/'+checkListId+'/checkItems/'+listId+'?key='+key+'&token='+tokenKey),{
            method:'DELETE',
        })
        .then((res)=>{
            return res.json();
        })

      }
      async function updatingCheckListItem(cardId,checkListItemId){
          console.log("working");
          return await fetch(('https://api.trello.com/1/cards/'+cardId+'/checkItem/'+checkListItemId+'?&state=complete&key='+key+'&token='+tokenKey),{
              method:'PUT',
          })
          .then((res)=>{
              return res.json();
          })
      }
      async function deletingCards(cardId){
        return await fetch(('https://api.trello.com/1/cards/'+cardId+'?closed=true&key='+key+'&token='+tokenKey),{
     method:'PUT',
        })
        .then((res)=>{
            return res.json();
        })
      }
      async function fetchingItems(cardId,itemId){
          return await fetch(('https://api.trello.com/1/cards/'+cardId+'/checkItem/'+itemId+'?state=incomplete&key='+key+'&token='+tokenKey),{
              method:'PUT',
          })
          .then((res)=>{
              return res.json();
          })
      }
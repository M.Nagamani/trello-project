  function gettingCards() {
    let cardListArray = [];
    let cardListIdArray = [];
    gettingCardListId()
        .then(function(data){
            data.forEach((element) => {
                cardListIdArray.push([element.name, element.id])
                cardListArray.push(element.name);
            })
            addingCards(cardListArray);
            updatingBoard(cardListArray);
          cardButtonClicked(cardListIdArray);
        })
}
gettingCards();
function addingCards(cardListArray) {
    let addButton = document.querySelector(".button");
    addButton.addEventListener("click", function (event) {
        let inputText = document.querySelector(".my-text").value;
        if (inputText == "") {
            alert("please enter the text first")
        }
        else {
            cardListArray.push(inputText);
            insertingCards(inputText);
            document.querySelector(".my-text").value = "";
            updatingBoard(cardListArray);
        }
    })
}
function updatingBoard(cardListArray) {
    document.querySelector(".add-checklist").innerHTML = "";
    cardListArray.forEach((data) => {
        let newnode = document.createTextNode(data);
        let mainDiv = document.querySelector(".add-checklist");
        let checklistDiv = document.createElement("div");
        checklistDiv.append(newnode);
        mainDiv.append(checklistDiv);
    })
}
function cardButtonClicked(cardListIdArray) {
    let cardItem = document.querySelector(".add-checklist");
    cardItem.addEventListener("click", function (event) {
        var modal = document.getElementById("simpleModal");
        let modalContent = document.createElement("div");
        modalContent.className = "modal-content";
        modal.style.display = "block";
        let checklistItemDiv = document.createElement("div");
        let insidemodalDiv = document.createElement("div");
        insidemodalDiv.className = "checklist-div";
        let mainChecklistButton = document.createElement("button");
        mainChecklistButton.innerHTML = "Add";
        mainChecklistButton.className = "button-checklist";
        let mainInputChecklist = document.createElement("input");
        mainInputChecklist.type = "text";
        mainInputChecklist.className = "input-checklist";
        let closeBtn = document.createElement("span");
        closeBtn.className = "closeBtn";
        closeBtn.innerHTML = "&times;";
         let archeiveButton=document.createElement("button");
        archeiveButton.innerHTML = "Archeive";
        archeiveButton.className = "archeive-btn";
        checklistItemDiv.append(mainInputChecklist);
        checklistItemDiv.append(mainChecklistButton);
        checklistItemDiv.append(closeBtn);
        modalContent.append(checklistItemDiv)
        modalContent.append(insidemodalDiv);
        modalContent.append(archeiveButton);
        modal.append(modalContent);
        archeiveButton.addEventListener("click",  function () {
           cardListIdArray.forEach(async function (element) {
                if (element[0] == event.target.innerHTML) {
                    await deletingCards(element[1]);
                    await domRefresh();
                    modal.style.display = "none";
                    await  gettingCards();
                }
            })
        })
        fetchingchecklist(cardListIdArray, event.target)
        gettingChecklistInput(cardListIdArray, event.target);
        closeBtn.addEventListener("click", function () {
            domRefresh();
            modal.style.display = "none";
        })
    })
}
function domRefresh() {
    document.querySelector("#simpleModal").innerHTML = "";
}
function fetchingchecklist(cardListIdArray, clickedCard) {
    cardListIdArray.forEach((element) => {
        if (element[0] == clickedCard.innerHTML) {
            gettingChecklist(element[1])
                .then((data) => {
                    data.forEach((element) => {
                        addingChecklist(element.name, element.id, cardListIdArray, clickedCard);

                    })
                })
        }
    })
}
function domRefreshCheckList() {
    document.querySelector(".checklist-div").innerHTML = "";
}
function gettingChecklistInput(cardListIdArray, clickedCard) {
    document.querySelector(".button-checklist").addEventListener("click", function () {
        let inputValue = document.querySelector(".input-checklist").value;
        if (inputValue == "") {
            alert("enter the first text");
        }
        else {
            cardListIdArray.forEach(async function (element) {
                if (element[0] == clickedCard.innerHTML) {
                    document.querySelector(".input-checklist").value = "";
                    await insertingChecklist(element[1], inputValue);
                    await domRefreshCheckList();
                    await fetchingchecklist(cardListIdArray, clickedCard);
                }
            })
        }
    })
}
function addingChecklist(checkListName, checkListId, cardListIdArray, clickedCard) {
    let mainChecklistDiv = document.createElement("div");
    mainChecklistDiv.className = "main-checklist"
    let inputCheckListSpan = document.createElement("span");
    inputCheckListSpan.className = "items-add";
    let checkListButton = document.createElement("button");
    checkListButton.innerHTML = "Add";
    checkListButton.className = "checklist-btn";
    let checkListInput = document.createElement("input");
    checkListInput.type = "text";
    checkListInput.className = "checklist-input";
    let checkListDelete = document.createElement("button");
    checkListDelete.innerHTML = "Delete";
    checkListDelete.className = "check-delete";
    let checkItemList = document.createElement("span");
    checkItemList.innerHTML = checkListName;
    let checkList = document.querySelector(".checklist-div");
    let checkListItemsDiv = document.createElement("ul");
    checkListItemsDiv.className = "checklist-items";
    mainChecklistDiv.append(checkItemList);
    inputCheckListSpan.append(checkListInput);
    inputCheckListSpan.append(checkListButton);
    inputCheckListSpan.prepend(checkListDelete);
    mainChecklistDiv.append(inputCheckListSpan);
    mainChecklistDiv.append(checkListItemsDiv);
    checkList.append(mainChecklistDiv);
    cardListIdArray.forEach((element)=>{
        if(element[0]==clickedCard.innerHTML){
            fetchingchecklistItems(checkListId, checkListItemsDiv,element[1]);  
        }
    })
    checkListDelete.addEventListener("click", async function (event) {
        alert("Do you want to delete");
        let deleteChecklistName = event.target.parentNode.parentNode;
        if (checkListName == deleteChecklistName.firstChild.innerHTML) {
            await deleteCheckList(checkListId);
            await domRefreshCheckList();
            await fetchingchecklist(cardListIdArray, clickedCard)
        }
    })
    cardListIdArray.forEach((element)=>{
        if(element[0]==clickedCard.innerHTML){
    checkListButton.addEventListener("click", async function (event) {
        let inputItemsValue = event.target.parentNode.parentNode.querySelector(".checklist-input").value;
        if (inputItemsValue == "") {
            alert("please enter the item");
        }
        else {
            event.target.parentNode.parentNode.querySelector(".checklist-input").value = "";
            await insertingChecklistItems(checkListId, inputItemsValue);
            await domRefreshItems(event.target.parentNode.nextSibling);
            await fetchingchecklistItems(checkListId, event.target.parentNode.nextSibling,element[1]);
        }
    })
}
})
}
function domRefreshItems(refreshDiv) {
    refreshDiv.innerHTML = "";
}
function fetchingchecklistItems(checkListId, checkListDiv, clickedCardId) {
    gettingListItems(checkListId)
        .then((data) => {
            data.forEach((element) => {
                addingCheckListItems(element.name, element.id, checkListDiv, checkListId, clickedCardId);
            })
        })
}
function addingCheckListItems(itemName, itemId, checkListDiv, checkListId, clickedCardId) {
    let checkItemsList = document.createElement("li");
    let checkItemsSpan = document.createElement("span");
    checkItemsSpan.innerHTML = itemName;
    let checkItemsCheckBox = document.createElement("input");
    checkItemsCheckBox.type = "checkbox";
    checkItemsCheckBox.className = "check-box";
    let checkItemsDelete = document.createElement("button");
    checkItemsDelete.innerHTML = "Delete";
    checkItemsDelete.className = "delete-items";
    checkItemsList.append(checkItemsCheckBox);
    checkItemsList.append(checkItemsSpan);
    checkItemsList.append(checkItemsDelete);
    checkListDiv.append(checkItemsList);
    checkItemsCheckBox.addEventListener("change",async  function (event) {
        if (event.target.checked) {
                  await updatingCheckListItem(clickedCardId, itemId);
                 event.target.nextSibling.style.textDecoration = "line-through";
        }
        else {
            await fetchingItems(clickedCardId,itemId);
           event.target.nextSibling.style.textDecoration = "none";
        }
    })
    checkItemsDelete.addEventListener("click", async function (event) {
        alert("Do you want to delete");
        let deleteCheckListItem = event.target.parentNode.querySelector("span").innerHTML;
        if (itemName == deleteCheckListItem) {
            await deleteCheckListItems(checkListId, itemId);
            await domRefreshItems(event.target.parentNode);
            await fetchingchecklistItems(checkListId, event.target.parentNode, clickedCardId)
        }
    })
}